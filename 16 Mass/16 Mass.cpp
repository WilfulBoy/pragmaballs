﻿#include <iostream>
#include <ctime> 
#include <time.h>
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

using namespace std;

int main() 
{
    const int N = 5;
    int sum;
    
    
        
    int array[N][N];

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cout << array[i][j] << " ";
        }
        cout << endl;
    }

    time_t theTime = time(NULL);
    struct tm* aTime = localtime(&theTime);

    int day = aTime->tm_mday;
    
    
    
    for (int i = 0; i < N; i++)
    {
        sum = 0;
        if (day % N != i)
            continue;
        for (int j = 0; j < N; j++)
            sum += array[i][j];
        cout << i << "=" << sum << endl;
    }
    cout << "today is " << day;
    return 0;
}